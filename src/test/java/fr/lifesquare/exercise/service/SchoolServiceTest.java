package fr.lifesquare.exercise.service;

import fr.lifesquare.exercise.model.SchoolClass;
import fr.lifesquare.exercise.model.SchoolStudent;
import fr.lifesquare.exercise.repository.SchoolClassRepository;
import fr.lifesquare.exercise.repository.SchoolStudentRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class SchoolServiceTest {
    @Mock
    private SchoolClassRepository schoolClassRepository;
    @Mock
    private SchoolStudentRepository schoolStudentRepository;

    @InjectMocks
    private SchoolServiceImpl schoolService;


    @Test
    public void test_findAllStudentBySchoolClass() {
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setName("class1");
        schoolClass.setLevel("level1");

        SchoolStudent schoolStudent = new SchoolStudent();
        schoolStudent.setFirstName("Gérard");
        schoolStudent.setLastName("Dupont");
        schoolStudent.setBirthDate(new Date());
        schoolStudent.setSchoolClass(schoolClass);

        schoolClassRepository.save(schoolClass);
        schoolStudentRepository.save(schoolStudent);

        List<SchoolStudent> schoolStudentList = schoolService.findAllStudentBySchoolClass(1L);
        assertEquals(1, schoolStudentList.size());
    }

}
