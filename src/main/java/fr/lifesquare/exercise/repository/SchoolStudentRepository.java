package fr.lifesquare.exercise.repository;

import fr.lifesquare.exercise.model.SchoolClass;
import fr.lifesquare.exercise.model.SchoolStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolStudentRepository extends JpaRepository<SchoolStudent, Long> {
    List<SchoolStudent> findAllBySchoolClass(Long schoolClassId);
}
