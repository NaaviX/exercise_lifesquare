package fr.lifesquare.exercise.repository;

import fr.lifesquare.exercise.model.SchoolClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolClassRepository  extends JpaRepository<SchoolClass, Long> {
}
