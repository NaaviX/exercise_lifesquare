package fr.lifesquare.exercise.controller;

import fr.lifesquare.exercise.model.SchoolStudent;
import fr.lifesquare.exercise.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/school")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping(value = "/students/{schoolClassId}")
    public List<SchoolStudent> getStudentsBySchoolClass(@PathVariable Long schoolClassId) {
        List<SchoolStudent> schoolStudentList = schoolService.findAllStudentBySchoolClass(schoolClassId);
        return schoolStudentList;
    }

    @PutMapping(value = "/student/{schoolClassId}")
    public SchoolStudent addStudent(@PathVariable Long schoolClassId, @RequestParam SchoolStudent schoolStudent) {
        return schoolService.addStudent(schoolClassId, schoolStudent);
    }
}
