package fr.lifesquare.exercise.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "school_class")
public class SchoolClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String level;
}
