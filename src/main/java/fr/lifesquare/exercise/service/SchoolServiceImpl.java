package fr.lifesquare.exercise.service;

import fr.lifesquare.exercise.model.SchoolStudent;
import fr.lifesquare.exercise.repository.SchoolClassRepository;
import fr.lifesquare.exercise.repository.SchoolStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchoolServiceImpl implements SchoolService {

    @Autowired
    private SchoolClassRepository schoolClassRepository;

    @Autowired
    private SchoolStudentRepository schoolStudentRepository;

    @Override
    public List<SchoolStudent> findAllStudentBySchoolClass(Long id) {
        return schoolStudentRepository.findAllBySchoolClass(id);
    }

    @Override
    public SchoolStudent addStudent(Long schoolClassId, SchoolStudent schoolStudent) {
        schoolStudent.setSchoolClass(schoolClassRepository.findById(schoolClassId).orElse(null));
        return schoolStudentRepository.save(schoolStudent);
    }
}
