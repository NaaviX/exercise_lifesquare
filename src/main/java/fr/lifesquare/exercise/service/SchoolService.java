package fr.lifesquare.exercise.service;

import fr.lifesquare.exercise.model.SchoolClass;
import fr.lifesquare.exercise.model.SchoolStudent;

import java.util.List;

public interface SchoolService {
    List<SchoolStudent> findAllStudentBySchoolClass(final Long id);

    SchoolStudent addStudent(Long schoolClassId, SchoolStudent schoolStudent);
}
